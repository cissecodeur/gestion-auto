import React , { Component } from "react";

export class Reglages extends Component {

    render(){
          const title = "Reglages" 
        return(
                                      
            <div className="content-wrapper">
            {/* Content Header (Page header) */}
            <section className="content-header">
              <h1>
                  { title }
              </h1>           
            </section>
            {/* Main content */}
            
            <section className="content">
              <div className="row">
                <div className="col-xs-12">
                  <div className="box">
                    <div className="box-header">
                      <h3 className="box-title">Hover Data Table</h3>
                    </div>
                    {/* /.box-header */}
                    <div className="box-body">
                      <table id="example2" className="table table-bordered table-hover">
                        <thead>
                          <tr>
                            <th>Rendering engine</th>
                            <th>Browser</th>
                            <th>Platform(s)</th>
                            <th>Engine version</th>
                            <th>CSS grade</th>
                          </tr>
                        </thead>
                        <tbody>                      
                          <tr>
                            <td>Misc</td>
                            <td>PSP browser</td>
                            <td>PSP</td>
                            <td>-</td>
                            <td>C</td>
                          </tr>
                          <tr>
                            <td>Other browsers</td>
                            <td>All others</td>
                            <td>-</td>
                            <td>-</td>
                            <td>U</td>
                          </tr>
                        </tbody>
                        <tfoot>
                          <tr>
                            <th>Rendering engine</th>
                            <th>Browser</th>
                            <th>Platform(s)</th>
                            <th>Engine version</th>
                            <th>CSS grade</th>
                          </tr>
                        </tfoot>
                      </table>
                    </div>
                    {/* /.box-body */}
                  </div>
                </div>
                {/* /.col */}
              </div>
              {/* /.row */}
            </section>
            {/* /.content */}
          </div>

          
        )
    }
}