import React , { Component } from "react";
import { ButtonToolbar, Button } from 'react-bootstrap';
import { AjouterpanneModal } from "../Ajouts/ajouterpanneModal";



export class Pannes extends Component {
  constructor(props){
    super(props);
    this.state = {             
            pannedetails:[],         
            idpanne:"",
            idchaufeur:"",
            idvehicule :"", 
            typepanne : "" ,
            datepanne: "", 
            datereparation :"",         
            visible: false,
            modifierModal: false
                                          }

   this.AfficherAll = this.AfficherAll.bind(this); 
   this.FermermodifierModal = this.FermermodifierModal.bind(this);
   this.FermerModal  = this.FermerModal.bind(this)   ;
   this.montreModal = this.montreModal.bind(this)                             

}

componentDidMount() {
      this.AfficherAll();
}

AfficherAll(){
    /*fetch('http://localhost:9191/pannes')
    .then(response=> response.json())
    .then(data =>{
     this.setState({pannedetails:data}); */
     this.setState({pannedetails:[
                 
             {"idVehicule":1,"modeleVehicule":"ML320","couleurVehicule":"rouge","nombreplaceVehicule" : 24,"confortVehicule":"Climatisée" }, 
             {"idVehicule":2,"modeleVehicule":"ML320","couleurVehicule":"noire","nombreplaceVehicule" : 26,"confortVehicule":"simple"},
             {"idVehicule":3,"modeleVehicule":"ML320","couleurVehicule":"marron","nombreplaceVehicule" : 27,"confortVehicule":"Climatisée"}
                  ]                           

               });
     }
//Fin de la consommation de la methode get
// fonction de supression
SupprimerPanne (idpanne) {
    if(window.confirm("Voulez vous supprimer cette panne ?"))
     {
        fetch('http://localhost:9191/api/pannes'+ idpanne,
        {
           method:'DELETE',
           Header:{'Accept':'application/json'},
           'Content-Type':'application/json'
        })
     }
}

  FermerModal() {
  this.setState({ visible : false })
  }

  FermermodifierModal (){
  this.setState({ montreMofifierModal: false }) ;
 } 

 montreModal(){
   this.setState({visible: true })
 }

 




render(){
    
          const  title ="Pannes des vehicules";
          const  { pannedetails } = this.state;
          const { pannedetail } = this.state ;
          const idpanne = this.state;
          const idchaufeur = this.state;
          const idvehicule = this.state;
          const typepanne  = this.state;
          const datepanne =  this.state;
          const datereparation = this.state        
          
          
    return(
                                
        <div className="content-wrapper">
        <section className="content-header">
          <h1>
              { title }
          </h1>           
        </section>            
        <section className="content">
          <div className="row">
            <div className="col-xs-12">
              <div className="box">
                <div className="box-header">
                  <h3 className="box-title">Pannes sur les vehicules</h3>
                </div>
                <div className="box-body">
                  <table className="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>ID VEHICULE</th>
                        <th>ID DU CHAUFFEUR</th>
                        <th>TYPE DE PANNE</th>
                        <th>DATE DE LA PANNE</th>
                        <th>DATE DE REPARATION</th>
                        <th>ACTIONS</th>
                      </tr>
                    </thead>
                    <tbody>                      
                        { pannedetails.map(pannedetail =>
                         <tr key = { pannedetail.idPanne }>
                          <td>{ pannedetail.idPanne }</td>
                          <td>{ pannedetail.idVehicule }</td>
                          <td>{ pannedetail.idChauffeur }</td>
                          <td>{ pannedetail.typePanne }</td>
                          <td>{ pannedetail.datePanne }</td>
                          <td>{ pannedetail.dateReparation }</td>
                                             
                         
                         
                          <td>
                            <ButtonToolbar> 
                              <Button
                                  className ="mr-2" 
                                  variant ="info"
                                  onClick = { () => this.setState({
                                    montreMofifierModal: true ,
                                    idpanne  : pannedetail.idPanne,
                                    idchaufeur: pannedetail.idChaufeur ,
                                    idvehicule: pannedetail.idVehicule,
                                    typepanne: pannedetail.typePanne,
                                    datepanne: pannedetail.datePanne,     
                                    dateReparation: pannedetail.dateReparation 
                                    
                                                                
                                    }
                               )}
                              > 
                                Modifier
                              </Button> 

                              <Button
                                     className ="mr-2"
                                     onClick = {() =>{this.SupprimerPanne(pannedetail.idpanne)} }
                                     variant = "danger"
                                     >
                                   Supprimer
                                    </Button>
                            </ButtonToolbar>   
                          </td>       
                         </tr>  
                            )}  
                    </tbody>                                        
                  </table>
                </div>
              </div>
            </div>
           
          </div>
          <ButtonToolbar>
             <Button variant = "success" onClick = { this.montreModal }>Enregistrer la panne</Button>
            <AjouterpanneModal  show = { this.props.visible } onHide = { this.FermerModal } />
          </ButtonToolbar>
        </section>
        
                             
      </div>
      
    )
}
}