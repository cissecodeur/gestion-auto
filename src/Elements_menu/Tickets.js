import React , { Component } from "react";
import { ButtonToolbar, Button } from 'react-bootstrap';
import { AjouterticketModal } from "../Ajouts/AjouterticketModal";


export class Tickets extends Component {
  constructor(props){
    super(props);
    this.state = {             
            ticketdetails:[],         
            idticket:"",
            email :"",
            
            visible: false,
            modifierModal: false
                                          }

   this.AfficherAll = this.AfficherAll.bind(this); 
   this.FermermodifierModal = this.FermermodifierModal.bind(this);
   this.FermerModal  = this.FermerModal.bind(this)   ;
   this.montreModal = this.montreModal.bind(this)                             

}

componentDidMount() {
      this.AfficherAll();
}

AfficherAll(){
    /*fetch('http://localhost:9191/locataires')
    .then(response=> response.json())
    .then(data =>{
     this.setState({locatairedetails:data}); */
     this.setState({ticketdetails:[
                 
             {"idVehicule":1,"modeleVehicule":"ML320","couleurVehicule":"rouge","nombreplaceVehicule" : 24,"confortVehicule":"Climatisée" }, 
             {"idVehicule":2,"modeleVehicule":"ML320","couleurVehicule":"noire","nombreplaceVehicule" : 26,"confortVehicule":"simple"},
             {"idVehicule":3,"modeleVehicule":"ML320","couleurVehicule":"marron","nombreplaceVehicule" : 27,"confortVehicule":"Climatisée"}
                  ]                           

               });
     }
//Fin de la consommation de la methode get
// fonction de supression
SupprimerTicket (idticket) {
    if(window.confirm("Voulez vous supprimer ce ticket ?"))
     {
        fetch('http://localhost:9191/api/tickets'+ idticket,
        {
           method:'DELETE',
           Header:{'Accept':'application/json'},
           'Content-Type':'application/json'
        })
     }
}

  FermerModal() {
  this.setState({ visible : false })
  }

  FermermodifierModal (){
  this.setState({ montreMofifierModal: false }) ;
 } 

 montreModal(){
   this.setState({visible: true })
 }

 




render(){
    
          const  title ="Tickets";
          const  { ticketdetails } = this.state;
          const { ticketdetail } = this.state ;
          const idticket = this.state;
          const email = this.state;
          
          
    return(
                                
        <div className="content-wrapper">
        <section className="content-header">
          <h1>
              { title }
          </h1>           
        </section>            
        <section className="content">
          <div className="row">
            <div className="col-xs-12">
              <div className="box">
                <div className="box-header">
                  <h3 className="box-title">Listes des tickets</h3>
                </div>
                <div className="box-body">
                  <table className="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th className = "mr3">ID</th>
                        <th className = "mr6">EMAIL</th>
                        <th className = "mr3">ACTIONS</th>
                      </tr>
                    </thead>
                    <tbody>                      
                        { ticketdetails.map(ticketdetail =>
                         <tr key = { ticketdetail.idTicket }>
                          <td>{ ticketdetail.idTicket }</td>
                          <td>{ ticketdetail.Email }</td>                             
                          <td>
                            <ButtonToolbar> 
                              <Button
                                  className ="mr-2" 
                                  variant ="info"
                                  onClick = { () => this.setState({
                                    montreMofifierModal: true ,
                                    idticket: ticketdetail.idTicket ,
                                    email: ticketdetail.Email,
                                   
                                                                
                                    }
                               )}
                              > 
                                Repondre
                              </Button> 

                              <Button
                                     className ="mr-2"
                                     onClick = {() =>{this.SupprimerTicket(ticketdetails.idticket)} }
                                     variant = "danger"
                                     >
                                   Supprimer
                                    </Button>
                            </ButtonToolbar>   
                          </td>       
                         </tr>  
                            )}  
                    </tbody>                                        
                  </table>
                </div>
              </div>
            </div>
           
          </div>
          <ButtonToolbar>
             <Button variant = "success" onClick = { this.montreModal }>Ouvrir un tichet</Button>
            <AjouterticketModal show = { this.props.visible } onHide = { this.FermerModal } />
          </ButtonToolbar>
        </section>
        
                             
      </div>
      
    )
}
}