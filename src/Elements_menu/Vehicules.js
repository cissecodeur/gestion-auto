import React , { Component } from "react";
import { ButtonToolbar, Button } from 'react-bootstrap';
import { AjoutervehiculeModal } from '../Ajouts/AjoutervehiculeModal';



export class Vehicules extends Component {
    constructor(props){
        super(props);
        this.state = {             
                vehicules:[{id : "1",
                modele: "fiat",
                couleur : "rouge",
                place : "32",
                confort : "Climatisée"}],                                                                     
    }
  }
 
    //Fin de la consommation de la methode get
    // fonction de supression
    handleDelete = (id) => {
        if(window.confirm("Voulez vous supprimer ce vehicule ?"))
         {
            const vehicules = this.state.vehicules.slice();
            const index  =  vehicules.findIndex(function(vehicule){    //chercher l'index
                return  vehicule.id === id;
            });
           vehicules.splice(index , 1);
           this.setState({ vehicules : vehicules })
         }
    }
    


    
    render(){
              const AddModalClose = ()  => {
              this.setState({ AddModalShow: false } )
                    };
        
              const  title ="Stock des vehicules";
              const elements = 
              <tbody>
              { this.state.vehicules.map(vehicule =>       
               <tr key = { vehicule.id }>
                <td>{ vehicule.id }</td>
                <td>{ vehicule.modele }</td>
                <td>{ vehicule.couleur}</td>
                <td>{ vehicule.place }</td>
                <td>{ vehicule.confort }</td>
                <td>
                  <ButtonToolbar> 
                    <Button
                        className ="mr-2" 
                        variant ="info"
                    > 
                      Modifier
                    </Button> 

                    <Button
                           className ="mr-2 xl"
                           onClick = {() => this.handleDelete(vehicule.id) } 
                           variant = "danger"
                           >
                         Supprimer
                          </Button>
                  </ButtonToolbar>   
                </td>       
               </tr>   
                  )}  
            </tbody> 
            

        return(
                                    
            <div className="content-wrapper">
            <section className="content-header">
              <h1>
                  { title }
              </h1>   
              <ButtonToolbar>
                 <Button className = "btn-circle.btn-md" 
                    onClick = { () => this.setState({ AddModalShow: true }) } 
                    variant = "success">Ajouter Vehicule
                 </Button>
              </ButtonToolbar>          
            </section>  
                    
            <section className="content">
              <div className="row">
                <div className="col-xs-12">
                  <div className="box">
                    <div className="box-header">
                      <h3 className="box-title">Disponibilité des vehicules</h3>
                    </div>
                    <div className="box-body">
                      <table className="table table-bordered table-hover">
                        <thead>
                          <tr>
                            <th>NUMERO</th>
                            <th>MODELE</th>
                            <th>COULEUR</th>
                            <th>NOMBRE DE PLACE</th>
                            <th>CONFORT</th>
                            <th>ACTIONS</th>
                          </tr>
                        </thead>
                             { elements }                             
                      </table>
                    </div>
                  </div>
                </div>
               
              </div>             
            </section>
            <AjoutervehiculeModal show = { this.state.AddModalShow } onHide = { AddModalClose }/>                             
          </div>
          
        )
    }
}