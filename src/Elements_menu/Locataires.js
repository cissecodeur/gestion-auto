import React , { Component } from "react";
import { ButtonToolbar, Button } from 'react-bootstrap';
import { AjouterlocataireModal } from "../Ajouts/AjouterlocataireModal";

export class Locataires extends Component {
  constructor(props){
    super(props);
    this.state = {             
            locatairedetails:[],
            idlocataire : "",
            nomlocataire: "",
            prenomlocataire: "",
            contactlocataire : "",
            residencelocataire : "",
            idvehicule :"",
            dateemprunt : "",
            dateretour: "",
            idchaufeur:"",
            
            visible: false,
            modifierModal: false
                                          }

   this.AfficherAll = this.AfficherAll.bind(this); 
   this.FermermodifierModal = this.FermermodifierModal.bind(this);
   this.FermerModal  = this.FermerModal.bind(this)   ;
   this.montreModal = this.montreModal.bind(this)                             

}

componentDidMount() {
      this.AfficherAll();
}

AfficherAll(){
    /*fetch('http://localhost:9191/locataires')
    .then(response=> response.json())
    .then(data =>{
     this.setState({locatairedetails:data}); */
     this.setState({locatairedetails:[
                 
             {"idVehicule":1,"modeleVehicule":"ML320","couleurVehicule":"rouge","nombreplaceVehicule" : 24,"confortVehicule":"Climatisée" }, 
             {"idVehicule":2,"modeleVehicule":"ML320","couleurVehicule":"noire","nombreplaceVehicule" : 26,"confortVehicule":"simple"},
             {"idVehicule":3,"modeleVehicule":"ML320","couleurVehicule":"marron","nombreplaceVehicule" : 27,"confortVehicule":"Climatisée"}
                  ]                           

               });
     }
//Fin de la consommation de la methode get
// fonction de supression
SupprimerLocataire (idlocataire) {
    if(window.confirm("Voulez vous supprimer ce vehicule ?"))
     {
        fetch('http://localhost:9191/api/locataires'+ idlocataire,
        {
           method:'DELETE',
           Header:{'Accept':'application/json'},
           'Content-Type':'application/json'
        })
     }
}

  FermerModal() {
  this.setState({ visible : false })
  }

  FermermodifierModal (){
  this.setState({ montreMofifierModal: false }) ;
 } 

 montreModal(){
   this.setState({visible: true })
 }

 




render(){
    
          const  title ="Locataires des vehicules";
          const  { locatairedetails } = this.state;
          const { locatairedetail } = this.state ;
          const idlocataire =  this.state;
          const nomlocataire = this.state;
          const prenomlocataire = this.state;
          const contactlocataire = this.state;
          const residencelocataire = this.state;
          const idvehicule = this.state;
          const dateemprunt = this.state;
          const dateretour = this.state;
          const idchaufeur = this.state;
    return(
                                
        <div className="content-wrapper">
        <section className="content-header">
          <h1>
              { title }
          </h1>           
        </section>            
        <section className="content">
          <div className="row">
            <div className="col-xs-12">
              <div className="box">
                <div className="box-header">
                  <h3 className="box-title">Locataires des vehicules</h3>
                </div>
                <div className="box-body">
                  <table className="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>ID VEHICULE</th>
                        <th>ID CHAUFFEUR</th>
                        <th>NOM</th>
                        <th>PRENOM</th>
                        <th>CONTACT</th>
                        <th>RESIDENCE</th>
                        <th>DATE EMPRUNT</th>
                        <th>DATE DE RETOUR</th>
                        <th>ACTIONS</th>
                      </tr>
                    </thead>
                    <tbody>                      
                        { locatairedetails.map(locatairedetail =>
                         <tr key = { locatairedetail.idLocataire }>
                          <td>{ locatairedetail.idLocataire }</td>
                          <td>{ locatairedetail.idVehicule }</td>
                          <td>{ locatairedetail.idChaufeur }</td>
                          <td>{ locatairedetail.nomLocataire }</td>
                          <td>{ locatairedetail.prenomLocataire }</td>
                          <td>{ locatairedetail.contactLocataire }</td>
                          <td>{ locatairedetail.residenceLocataire }</td>                        
                          <td>{ locatairedetail.dateEmprunt }</td>
                          <td>{ locatairedetail.dateRetour }</td>
                         
                          <td>
                            <ButtonToolbar> 
                              <Button
                                  className ="mr-2" 
                                  variant ="info"
                                  onClick = { () => this.setState({
                                    montreMofifierModal: true ,
                                    idlocataire: locatairedetail.idLocataire ,
                                    idvehicule: locatairedetail.idVehicule,
                                    idchaufeur: locatairedetail.idChaufeur,
                                    nomlocataire: locatairedetail.nomLocataire,
                                    prenomlocataire: locatairedetail.prenomLocataire,     
                                    contactlocataire: locatairedetail.contactLocataire,  
                                    residencelocataire: locatairedetail.residenceLocataire,
                                    dateemprunt: locatairedetail.dateEmprunt,  
                                    dateretour: locatairedetail.dateRetour,                            
                                    }
                               )}
                              > 
                                Modifier
                              </Button> 

                              <Button
                                     className ="mr-2"
                                     onClick = {() =>{this.SupprimerLocataire(locatairedetail.idlocataire)} }
                                     variant = "danger"
                                     >
                                   Supprimer
                                    </Button>
                            </ButtonToolbar>   
                          </td>       
                         </tr>  
                            )}  
                    </tbody>                                        
                  </table>
                </div>
              </div>
            </div>
           
          </div>
          <ButtonToolbar>
             <Button variant = "success" onClick = { this.montreModal }>Ajouter locataire</Button>
            <AjouterlocataireModal show = { this.props.visible } onHide = { this.FermerModal } />
          </ButtonToolbar>
        </section>
        
                             
      </div>
      
    )
}
}