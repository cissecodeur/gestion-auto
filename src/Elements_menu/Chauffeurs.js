import React , { Component } from "react";
import { ButtonToolbar, Button } from 'react-bootstrap';
import { AjouterchauffeurModal } from "../Ajouts/AjouterchauffeurModal";


export class Chauffeurs extends Component {
  constructor(props){
    super(props);
    this.state = {             
            chauffeurdetails:[],         
            idchaufeur:"",
            idvehicule :"",
            nomchauffeur: "",
            prenomchauffeur: "",
            contactchauffeur : "",
            residencechauffeur : "",
                       
            visible: false,
            modifierModal: false
                                          }

   this.AfficherAll = this.AfficherAll.bind(this); 
   this.FermermodifierModal = this.FermermodifierModal.bind(this);
   this.FermerModal  = this.FermerModal.bind(this)   ;
   this.montreModal = this.montreModal.bind(this)                             

}

componentDidMount() {
      this.AfficherAll();
}

AfficherAll(){
    /*fetch('http://localhost:9191/locataires')
    .then(response=> response.json())
    .then(data =>{
     this.setState({locatairedetails:data}); */
     this.setState({chauffeurdetails:[
                 
             {"idVehicule":1,"modeleVehicule":"ML320","couleurVehicule":"rouge","nombreplaceVehicule" : 24,"confortVehicule":"Climatisée" }, 
             {"idVehicule":2,"modeleVehicule":"ML320","couleurVehicule":"noire","nombreplaceVehicule" : 26,"confortVehicule":"simple"},
             {"idVehicule":3,"modeleVehicule":"ML320","couleurVehicule":"marron","nombreplaceVehicule" : 27,"confortVehicule":"Climatisée"}
                  ]                           

               });
     }
//Fin de la consommation de la methode get
// fonction de supression
SupprimerChauffeur (idchaufeur) {
    if(window.confirm("Voulez vous supprimer ce chauffeur ?"))
     {
        fetch('http://localhost:9191/api/chauffeurs'+ idchaufeur,
        {
           method:'DELETE',
           Header:{'Accept':'application/json'},
           'Content-Type':'application/json'
        })
     }
}

  FermerModal() {
  this.setState({ visible : false })
  }

  FermermodifierModal (){
  this.setState({ montreMofifierModal: false }) ;
 } 

 montreModal(){
   this.setState({visible: true })
 }

 




render(){
    
          const  title ="Chauffeurs des vehicules";
          const  { chauffeurdetails } = this.state;
          const { chauffeurdetail } = this.state ;
          const idchaufeur = this.state;
          const idvehicule = this.state;
          const nomchauffeur =  this.state;
          const prenomchauffeur = this.state;
          const contactchauffeur = this.state;
          const residencechauffeur = this.state;
          
          
    return(
                                
        <div className="content-wrapper">
        <section className="content-header">
          <h1>
              { title }
          </h1>           
        </section>            
        <section className="content">
          <div className="row">
            <div className="col-xs-12">
              <div className="box">
                <div className="box-header">
                  <h3 className="box-title">Chauffeurs des vehicules</h3>
                </div>
                <div className="box-body">
                  <table className="table table-bordered table-hover">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>ID VEHICULE</th>
                        <th>NOM</th>
                        <th>PRENOM</th>
                        <th>CONTACT</th>
                        <th>RESIDENCE</th>
                        <th>ACTIONS</th>
                      </tr>
                    </thead>
                    <tbody>                      
                        { chauffeurdetails.map(chauffeurdetail =>
                         <tr key = { chauffeurdetail.idChaufeur }>
                          <td>{ chauffeurdetail.idChaufeur }</td>
                          <td>{ chauffeurdetail.idVehicule }</td>
                          <td>{ chauffeurdetail.nomChauffeur }</td>
                          <td>{ chauffeurdetail.prenomChauffeur }</td>
                          <td>{ chauffeurdetail.contactChauffeur }</td>
                          <td>{ chauffeurdetail.residenceChauffeur }</td>
                                             
                         
                         
                          <td>
                            <ButtonToolbar> 
                              <Button
                                  className ="mr-2" 
                                  variant ="info"
                                  onClick = { () => this.setState({
                                    montreMofifierModal: true ,
                                    idchaufeur: chauffeurdetail.idChaufeur ,
                                    idvehicule: chauffeurdetail.idVehicule,
                                    nomchauffeur: chauffeurdetail.nomChauffeur,
                                    prenomchauffeur: chauffeurdetail.prenomChauffeur,     
                                    contactchauffeur: chauffeurdetail.contactChauffeur,  
                                    residencechauffeur: chauffeurdetail.residenceChauffeur,
                                                                
                                    }
                               )}
                              > 
                                Modifier
                              </Button> 

                              <Button
                                     className ="mr-2"
                                     onClick = {() =>{this.SupprimerChauffeur(chauffeurdetail.idchaufeur)} }
                                     variant = "danger"
                                     >
                                   Supprimer
                                    </Button>
                            </ButtonToolbar>   
                          </td>       
                         </tr>  
                            )}  
                    </tbody>                                        
                  </table>
                </div>
              </div>
            </div>
           
          </div>
          <ButtonToolbar>
             <Button variant = "success" onClick = { this.montreModal }>Ajouter Chauffeur</Button>
            <AjouterchauffeurModal  show = { this.props.visible } onHide = { this.FermerModal } />
          </ButtonToolbar>
        </section>
        
                             
      </div>
      
    )
}
}