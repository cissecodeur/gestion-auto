import React , { Component } from "react";
import { ButtonToolbar, Button } from 'react-bootstrap';
import { AjoutervehiculeModal } from '../Ajouts/AjoutervehiculeModal';



export class Vehicules extends Component {
    constructor(props){
        super(props);
        this.state = {             
                vehiculedetails:[],
                idvehicule : "",
                modelevehicule: "",
                couleurvehicule : "",
                nombreplacevehicule : "",
                confortvehicule : "",
                ajoutervehiculeModalShow: false,
                modifiervehiculeModalShow: false
                                              }

       this.AfficherAll = this.AfficherAll.bind(this); 
       this.modifiervehiculeModalCLose = this.modifiervehiculeModalCLose.bind(this);
       this.ajoutervehiculeModalCLose  = this.ajoutervehiculeModalCLose.bind(this)   ;
       this.appellModal = this.appellModal.bind(this)                             

    }

    componentDidMount() {
          this.AfficherAll();
    }

    AfficherAll(){
        /*fetch('http://localhost:9191/vendeurs')
        .then(response=> response.json())
        .then(data =>{
         this.setState({vendeurdetails:data}); */
         this.setState({vehiculedetails:[
                     
                 {"idVehicule":1,"modeleVehicule":"ML320","couleurVehicule":"rouge","nombreplaceVehicule" : 24,"confortVehicule":"Climatisée" }, 
                 {"idVehicule":2,"modeleVehicule":"ML320","couleurVehicule":"noire","nombreplaceVehicule" : 26,"confortVehicule":"simple"},
                 {"idVehicule":3,"modeleVehicule":"ML320","couleurVehicule":"marron","nombreplaceVehicule" : 27,"confortVehicule":"Climatisée"}
                      ]                           

                   });
         }
    //Fin de la consommation de la methode get
    // fonction de supression
    SupprimerVehicule (idvehicule) {
        if(window.confirm("Voulez vous supprimer ce vehicule ?"))
         {
            fetch('http://localhost:9191/api/produits'+ idvehicule,
            {
               method:'DELETE',
               Header:{'Accept':'application/json'},
               'Content-Type':'application/json'
            })
         }
    }
    
     ajoutervehiculeModalCLose =() =>{
      this.setState({ ajoutervehiculeModalShow : false })
      }

     modifiervehiculeModalCLose (){
      this.setState({ modifiervehiculeModalShow: false }) ;
     } 

     appellModal(){
       this.setState({ajoutervehiculeModalShow: true })
     }
   
    

    render(){
        
              const title = "Stock des vehicules";
              const  { vehiculedetails } = this.state;
              const { vehiculedetail } = this.state ;
              const { idvehicule } = this.state;
              const  modelevehicule = this.state;
              const  couleurvehicule = this.state;
              const  nombreplacevehicule = this.state;
              const  confortvehicule = this.state;
  

        return(
                                    
            <div className="content-wrapper">
            <section className="content-header">
              <h1>
                  { title }
              </h1>           
            </section>            
            <section className="content">
              <div className="row">
                <div className="col-xs-12">
                  <div className="box">
                    <div className="box-header">
                      <h3 className="box-title">Disponibilité des vehicules</h3>
                    </div>
                    <div className="box-body">
                      <table className="table table-bordered table-hover">
                        <thead>
                          <tr>
                            <th>NUMERO</th>
                            <th>MODELE</th>
                            <th>COULEUR</th>
                            <th>NOMBRE DE PLACE</th>
                            <th>CONFORT</th>
                          </tr>
                        </thead>
                        <tbody>                      
                            { vehiculedetails.map(vehiculedetail =>
                             <tr key = { vehiculedetail.idVehicule }>
                              <td>{ vehiculedetail.idVehicule }</td>
                              <td>{ vehiculedetail.modeleVehicule }</td>
                              <td>{ vehiculedetail.couleurVehicule }</td>
                              <td>{ vehiculedetail.nombreplaceVehicule }</td>
                              <td>{ vehiculedetail.confortVehicule }</td>
                              <td>
                                <ButtonToolbar> 
                                  <Button
                                      className ="mr-2" 
                                      variant ="info"
                                      onClick = { () => this.setState({
                                        modifiervehiculeModalShow: true ,
                                        idvehicule: vehiculedetail.idVehicule ,
                                        modelevehicule: vehiculedetail.modeleVehicule,
                                        couleurvehicule: vehiculedetail.couleurVehicule,
                                        nombreplacevehicule: vehiculedetail.nombreplaceVehicule,
                                        confortvehicule: vehiculedetail.confortVehicule,                               
                                        }
                                   )}
                                  > 
                                    Modifier
                                  </Button> 

                                  <Button
                                         className ="mr-2"
                                         onClick = {() =>{this.SupprimerVehicule(vehiculedetail.idvehicule)} }
                                         variant = "danger"
                                         >
                                       Supprimer
                                        </Button>
                                </ButtonToolbar>   
                              </td>       
                             </tr>  
                                )}  
                        </tbody>                                        
                      </table>
                    </div>
                  </div>
                </div>
               
              </div>
              <ButtonToolbar>
                 <Button variant = "success" onClick = { this.appellModal }>Ajouter Vehicule</Button>
                <AjoutervehiculeModal show = { this.props.ajoutervehiculeModalShow } onHide = { this.ajoutervehiculeModalCLose } />
              </ButtonToolbar>
            </section>
            
                                 
          </div>
          
        )
    }
}