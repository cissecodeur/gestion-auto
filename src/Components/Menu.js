import React, { Component } from 'react';
import { Elements_menu } from './Elements_menu';
import { Content } from 'react-mdl';
import { BrowserRouter, Route , Switch  } from 'react-router-dom';
import { Vehicules } from '../Elements_menu/Vehicules';
import { Locataires } from '../Elements_menu/Locataires';
import { Chauffeurs } from '../Elements_menu/Chauffeurs';
import { Pannes } from '../Elements_menu/Pannes';
import { Tickets } from '../Elements_menu/Tickets';
import { Reglages } from '../Elements_menu/Reglages'; 
import { Link } from '@material-ui/core';

 

export class Menu extends Component {

    render(){
        return(
  <BrowserRouter>
  <aside className="main-sidebar">
    {/* sidebar: style can be found in sidebar.less */}
    <section className="sidebar">
      {/* Sidebar user panel */}
      <div className="user-panel">
        <div className="pull-left image">
          <img src="dist/img/user2-160x160.jpg" className="img-circle" alt="User" />
        </div>
        <div className="pull-left info">
          <p>Cisse Yacoub</p>
          <Link to="fake_url"><i className="fa fa-circle text-success" /> En ligne</Link>
        </div>
      </div>

      {/* search form */}
      <form action="#" method="get" className="sidebar-form">
        <div className="input-group">
          <input type="text" name="q" className="form-control" placeholder="Search..." />
          <span className="input-group-btn">
            <button type="submit" name="search" id="search-btn" className="btn btn-flat">
              <i className="fa fa-search" />
            </button>
          </span>
        </div>
      </form>
      {/* /.search form */}

     {/* /inclusion de contenu */}

      <Content>
        <div className = "page-content" />    
        <Elements_menu 
           Vehicules  = "Vehicules"
           Chauffeurs = "Chauffeurs"
           Locataires = "Locataires"
           Pannes     = "Pannes"
           Tickets    = "Tickets"
           Reglages   = "Reglages"  

        />
      </Content>
    {/* /Fin de l'inclusion de contenu */}

      
    </section>   
  </aside>
    <Switch> 
      <Route  path ="/Vehicules" component = { Vehicules } exact />
      <Route  path ="/Locataires" component = { Locataires }/>
      <Route  path ="/Chauffeurs" component = { Chauffeurs }/>
      <Route  path ="/Pannes" component = { Pannes }/>
      <Route  path ="/Tickets" component = { Tickets }/> 
      <Route  path ="/Reglages" component = { Reglages }/>                                   
    </Switch> 
</BrowserRouter> 

        );
    }
}