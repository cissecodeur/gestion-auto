import React, { Component } from 'react';


export class Header extends Component {

    render(){
        return(
          <div>
  <header className="main-header">
    {/* Logo */}
    <a href="index2.html" className="logo">
      {/* mini logo for sidebar mini 50x50 pixels */}
      <span className="logo-mini"><b>A</b>LT</span>
      {/* logo for regular state and mobile devices */}
      <span className="logo-lg"><b>GEST</b>CARS</span>
    </a>
    {/* Header Navbar: style can be found in header.less */}
    <nav className="navbar navbar-static-top">
      {/* Sidebar toggle button*/}
      <a href="fake_url" className="sidebar-toggle" data-toggle="push-menu" role="button">
        <span className="sr-only">Toggle navigation</span>
      </a>
      {/* Navbar Right Menu */}
      <div className="navbar-custom-menu">
        <ul className="nav navbar-nav">
          {/* Messages: style can be found in dropdown.less*/}
          <li className="dropdown messages-menu">
            <ul className="dropdown-menu">
              <li>
                {/* inner menu: contains the actual data */}
                <ul className="menu">
                  <li>
                    <a href="fake_url">
                      <div className="pull-left">
                        <img src="dist/img/user4-128x128.jpg" className="img-circle" alt="User" />
                      </div>
                      <h4>
                        Developpeur
                        <small><i className="fa fa-clock-o" />Aujourd'hui</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="fake_url">
                      <div className="pull-left">
                        <img src="dist/img/user3-128x128.jpg" className="img-circle" alt="User" />
                      </div>
                      <h4>
                        Sales Department
                        <small><i className="fa fa-clock-o" /> Hier</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                  <li>
                    <a href="fake_url">
                      <div className="pull-left">
                        <img src="dist/img/user4-128x128.jpg" className="img-circle" alt="User" />
                      </div>
                      <h4>
                        Reviewers
                        <small><i className="fa fa-clock-o" /> 2 days</small>
                      </h4>
                      <p>Why not buy a new awesome theme?</p>
                    </a>
                  </li>
                </ul>
              </li>
              
            </ul>
          </li>
          
          
          <li className="dropdown user user-menu">
            <a href="fake_url" className="dropdown-toggle" data-toggle="dropdown">
              <img src="dist/img/user2-160x160.jpg" className="user-image" alt="User" />
              <span className="hidden-xs"> Cisse Yacoub</span>
            </a>
            <ul className="dropdown-menu">
              {/* User image */}
              <li className="user-header">
                <img src="dist/img/user2-160x160.jpg" className="img-circle" alt="User" />
                <p>
                  Cisse Yacoub - Developpeur Web
                  <small>membre depuis 2012</small>
                </p>
              </li>
              {/* Menu Footer*/}
              <li className="user-footer">
                <div className="pull-left">
                  <a href="fake_url" className="btn btn-default btn-flat">Profile</a>
                </div>
                <div className="pull-right">
                  <a href="fake_url" className="btn btn-default btn-flat">Deconnexion</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
</div>

        );
    }
}