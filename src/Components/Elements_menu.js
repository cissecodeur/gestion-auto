import React,{ Component } from 'react';
import { List , ListItem, ListItemContent} from 'react-mdl';
import { Link  } from 'react-router-dom';


export class Elements_menu extends Component {

    render(){
      return(

    <div>        
      <List>
      <ListItem>
       <Link to = "/Vehicules/">
        <ListItemContent icon ='directions_car'>{ this.props.Vehicules }</ListItemContent>
       </Link>
      </ListItem>

      <ListItem>
        <Link to = "/Locataires/">
         <ListItemContent icon="person">{ this.props.Locataires }</ListItemContent>
        </Link>
      </ListItem>

      <ListItem>
        <Link to = "/Chauffeurs/">
         <ListItemContent icon='people'>{ this.props.Chauffeurs }</ListItemContent>
        </Link>
      </ListItem>

      <ListItem>
        <Link to= "/Pannes/">
         <ListItemContent icon='report_problem'> { this.props.Pannes }</ListItemContent>
        </Link>
      </ListItem>

      <ListItem>
        <Link to = "/Tickets/">
         <ListItemContent icon='email'>{ this.props.Tickets }</ListItemContent>
        </Link>
      </ListItem>
      

      <ListItem>
        <Link to = "/Reglages/">
         <ListItemContent icon='settings'>{ this.props.Reglages }</ListItemContent>
        </Link>
      </ListItem>
     </List>              
    </div>        
       
        )
    }
}