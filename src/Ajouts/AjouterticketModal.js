import React , { Component } from 'react';
import { Button , Modal , Row , Col , Form } from 'react-bootstrap';
import IconButton from '@material-ui/core/Icon';
import Snackbar from '@material-ui/core/Snackbar';



export class AjouterticketModal extends Component {

    constructor(props){
        super(props);
        this.state =  { ouvertureSnackbar: false , snackbarMsg: ''};
        this.Ajoutervehicule = this.Ajoutervehicule.bind(this);
        this.snackbarClose = this.snackbarClose.bind(this);
    }

    snackbarClose = ()  =>  { this.setState({ ouvertureSnackbar : false});  }

     // Consomation de la methode Post de notre API
     Ajoutervehicule(event){
        event.preventDefault();
        fetch('http://localhost:9191/api/vehicules',
        {
          method: 'POST',
          headers:
            { 'Accept':'application/json',
              'Content-Type' : 'application/json'
              },

          body: JSON.stringify({
            idVehicule:null,
            modeleVehicule: event.target.modeleVehicule.value,
            couleurVehicule: event.target.couleurVehicule.value,
            nombreplaceVehicule: event.target.nombreplaceVehicule.value,
            confortVehicule: event.target.confortVehicule.value,
            })

        })
        .then(response => response.json())
        .then((resultat) => {
          // alert (resultat);
          this.setState({ouvertureSnackbar:true , snackbarMsg:resultat});

        },

        (error) => {
              //alert ("Echec");
            this.setState({ouvertureSnackbar:true , snackbarMsg:'Echec'});
        }

        )
}


    render(props){
        return(
            <div className = "container">
              <Snackbar
                    anchorOrigin={{
                      vertical: 'bottom',
                      horizontal: 'left',
                    }}
                    open={this.ouvertureSnackbar}
                    autoHideDuration={3000}
                    onClose={this.snackbarClose}
                    ContentProps={{
                      'aria-describedby': 'message-id',
                    }}
                    message={<span id="message-id" >{this.state.snackbarMsg}</span>}
                    action={[
                      <IconButton
                        key="close"
                        aria-label="close"
                        color="inherit"
                        onClick={this.snackbarClose}
                      > x
                      </IconButton>,
                    ]}
                  />

                <Modal
                   {...this.props}
                      size="lg"
                      aria-labelledby="contained-modal-title-vcenter"
                      centered
                      >
                  <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                      Ajouter un vehicule
                    </Modal.Title>
                  </Modal.Header>

                  <Modal.Body>

                      <Row>
                        <Col sm = {8}>
                          <Form onSubmit = { this.Ajoutervehicule } >
                              <Form.Group controlId = "modeleVehicule">
                                    <Form.Label>modele du vehicule</Form.Label>
                                    <Form.Control  type="text"  name="modeleVehicule" placeholder=" modele du vehicule"   required  />
                              </Form.Group>

                              <Form.Group controlId = "couleurVehicule">
                                    <Form.Label>couleur du vehicule</Form.Label>
                                    <Form.Control  type="text"  name="couleurVehicule" placeholder=" couleur du vehicule"   required  />
                              </Form.Group>

                              <Form.Group controlId = "nombreplaceVehicule">
                                    <Form.Label>Nombre de place</Form.Label>
                                    <Form.Control  type="text"  name="nombreplaceVehicule" placeholder=" nombre de places "   required  />
                              </Form.Group>

                              <Form.Group controlId = "confortVehicule">
                                    <Form.Label>Niveau de confort</Form.Label>
                                    <Form.Control  type="text"  name="confortVehicule" placeholder=" niveau de confort"   required  />
                              </Form.Group>

                              <Form.Group>
                                    <Button variant="primary" type="submit">
                                        Ajouter vehicule
                                    </Button>
                              </Form.Group>

                          </Form>
                        </Col>
                      </Row>
                  </Modal.Body>

                  <Modal.Footer>
                    <Button variant="danger" onClick={ this.props.onHide }>Fermer</Button>
                  </Modal.Footer>
              </Modal>
              </div>
        );
    }
}