import React , { Component } from 'react';
import { Button , Modal , Row , Col , Form } from 'react-bootstrap';
import IconButton from '@material-ui/core/Icon';
import Snackbar from '@material-ui/core/Snackbar';



export class AjouterchauffeurModal extends Component {

    constructor(props){
        super(props);
        this.state =  { ouvertureSnackbar: false , snackbarMsg: ''};
        this.Ajouterlocataire = this.Ajouterlocataire.bind(this);
        this.snackbarClose = this.snackbarClose.bind(this);
    }

    snackbarClose = ()  =>  { this.setState({ ouvertureSnackbar : false});  }

     // Consomation de la methode Post de notre API
     Ajouterlocataire(event){
        event.preventDefault();
        fetch('http://localhost:9191/api/locataires',
        {
          method: 'POST',
          headers:
            { 'Accept':'application/json',
              'Content-Type' : 'application/json'
              },

          body: JSON.stringify({
            idLocataire:null,  
            idVehicule:event.target.idVehicule.value,
            idChauffeur:event.target.idChauffeur.value,
            nomLocataire: event.target.nomLocataire.value,
            prenomLocataire: event.target.prenomLocataire.value,
            contactLocataire: event.target.contactLocataire.value,
            residenceLocataire: event.target.residenceLocataire.value,
            dateEmprunt: event.target.dateEmprunt.value,
            dateRetour : event.target.dateRetour.value
            })

        })
        .then(response => response.json())
        .then((resultat) => {
          // alert (resultat);
          this.setState({ouvertureSnackbar:true , snackbarMsg:resultat});

        },

        (error) => {
              //alert ("Echec");
            this.setState({ouvertureSnackbar:true , snackbarMsg:'Echec'});
        }

        )
}


    render(props){
        return(
            <div className = "container">
              <Snackbar
                    anchorOrigin={{
                      vertical: 'bottom',
                      horizontal: 'left',
                    }}
                    open={this.ouvertureSnackbar}
                    autoHideDuration={3000}
                    onClose={this.snackbarClose}
                    ContentProps={{
                      'aria-describedby': 'message-id',
                    }}
                    message={<span id="message-id" >{this.state.snackbarMsg}</span>}
                    action={[
                      <IconButton
                        key="close"
                        aria-label="close"
                        color="inherit"
                        onClick={this.snackbarClose}
                      > x
                      </IconButton>,
                    ]}
                  />

                <Modal
                   {...this.props}
                      size="lg"
                      aria-labelledby="contained-modal-title-vcenter"
                      centered
                      >
                  <Modal.Header closeButton>
                    <Modal.Title id="contained-modal-title-vcenter">
                      Ajouter un locataire
                    </Modal.Title>
                  </Modal.Header>

                  <Modal.Body>

                      <Row>
                        <Col sm = {8}>
                          <Form onSubmit = { this.Ajouterlocataire } >
                          <Form.Group controlId = "idVehicule">
                                    <Form.Label>id du vehicule</Form.Label>
                                    <Form.Control  type="text"  name="idVehicule" placeholder=" id du vehicule"   required  />
                              </Form.Group>

                              <Form.Group controlId = "idChauffeur">
                                    <Form.Label>nom du locataire</Form.Label>
                                    <Form.Control  type="text"  name="idChauffeur" placeholder=" id du Chauffeur"   required  />
                              </Form.Group>

                              <Form.Group controlId = "nomLocataire">
                                    <Form.Label>nom du locataire</Form.Label>
                                    <Form.Control  type="text"  name="nomLocataire" placeholder=" nom du locataire"   required  />
                              </Form.Group>

                              <Form.Group controlId = "prenomLocataire">
                                    <Form.Label>couleur du vehicule</Form.Label>
                                    <Form.Control  type="text"  name="prenomLocataire" placeholder=" prenom du locataire"   required  />
                              </Form.Group>

                              <Form.Group controlId = "contactLocataire">
                                    <Form.Label>Nombre de place</Form.Label>
                                    <Form.Control  type="text"  name="contactLocataire" placeholder=" contact du locataire "   required  />
                              </Form.Group>

                              <Form.Group controlId = "residenceLocataire">
                                    <Form.Label>Niveau de confort</Form.Label>
                                    <Form.Control  type="text"  name="residenceLocataire" placeholder="residence du locataire"   required  />
                              </Form.Group>

                              <Form.Group controlId = "dateEmprunt">
                                    <Form.Label>Niveau de confort</Form.Label>
                                    <Form.Control  type="text"  name="dateEmprunt" placeholder="date emprunt du vehicule"   required  />
                              </Form.Group>

                              <Form.Group controlId = "dateRetour">
                                    <Form.Label>Niveau de confort</Form.Label>
                                    <Form.Control  type="text"  name="dateRetour" placeholder="date de retour du vehicule"   required  />
                              </Form.Group>

                              <Form.Group>
                                    <Button variant="primary" type="submit">
                                        Ajouter locataire
                                    </Button>
                              </Form.Group>

                          </Form>
                        </Col>
                      </Row>
                  </Modal.Body>

                  <Modal.Footer>
                    <Button variant="danger" onClick={ this.props.onHide }>Fermer</Button>
                  </Modal.Footer>
              </Modal>
              </div>
        );
    }
}