import React from 'react';
import './App.css';
import { Header } from './Components/Header';
import { Menu } from './Components/Menu';
import { Footer } from './Components/Footer';


function App() {
  return (
     
        <div>
             <Header/>
             <Menu/>
             <Footer/> 
        </div>
                              
  
      
  
  );
}

export default App;
